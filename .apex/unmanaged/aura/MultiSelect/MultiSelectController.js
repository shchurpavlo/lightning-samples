({
    init: function(component, event, helper) {
        var sortedOptions = [];
        var options = component.get("v.options");
        options.sort(function compare(a,b) {
        if (a.value == 'All'){ return -1; }
        else if (a.value < b.value){ return -1; }
        if (a.value > b.value){ return 1; }
            return 0;
        });

        var sortValue = helper.sort(component, event, helper);
        if(sortValue != null){
            options.forEach(function(option){
                if(String(option.value).includes(String(sortValue))) {
                    sortedOptions.push(option);
                }
        });
        } else { sortedOptions = options; }

        component.set("v.options_", sortedOptions);
        var values = helper.getSelectedValues(component);
        helper.setInfoText(component, values);
    },

    handleClick: function(component, event, helper) {
      var mainDiv = component.find('main-div');
      $A.util.addClass(mainDiv, 'slds-is-open');
    },

    handleSelection: function(component, event, helper) {
      var item = event.currentTarget;
      if (item && item.dataset) {
        var value = item.dataset.value;
        var selected = item.dataset.selected;

        var options = component.get("v.options_");

        if (event.shiftKey) {
          options.forEach(function(element) {
            if (element.value == value) {
              element.selected = selected == "true" ? false : true;
            }
          });
        } else {
          options.forEach(function(element) {
            if (element.value == value) {
              element.selected = selected == "true" ? false : true;
            } else {
              element.selected = false;
            }
          });
          var mainDiv = component.find('main-div');
          $A.util.removeClass(mainDiv, 'slds-is-open');
        }
        component.set("v.options_", options);
        var values = helper.getSelectedValues(component);
        var labels = helper.getSelectedLabels(component);

        var objectTypes = helper.toggleAccountTable(component, options);
        console.log('objectTypes : ' + objectTypes);
        console.log('objectTypes : ' + objectTypes === null);
        if (objectTypes.length > 0) {
            component.find('childComponent').sampleMethod(objectTypes);
        } else {
            component.set("v.allowRecordsDisplay", false);
            helper.toastNoRecords();
        }
        helper.setInfoText(component, labels);
        helper.despatchSelectChangeEvent(component, values);

      }
    },

    handleMouseLeave: function(component, event, helper) {
      component.set("v.dropdownOver",false);
      var mainDiv = component.find('main-div');
      $A.util.removeClass(mainDiv, 'slds-is-open');
    },

    handleMouseEnter: function(component, event, helper) {
      component.set("v.dropdownOver",true);
    },

    invokeSort : function(component, event, helper) {
        helper.sort(component, event, helper);
    },

    handleMouseOutButton: function(component, event, helper) {
      window.setTimeout(
        $A.getCallback(function() {
          if (component.isValid()) {
            if (component.get("v.dropdownOver")) { return; }
            var mainDiv = component.find('main-div');
            $A.util.removeClass(mainDiv, 'slds-is-open');
          }
        }), 200
      );
    }
})