({
    onSearch : function(component, event, helper) {
        var boatTypeID = component.get('v.selectedBoatID'); 
        console.log('In onSearch function: ' + boatTypeID);
        var action = component.get("c.getBoats");
        action.setParams({boatTypeID : boatTypeID});
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('In onSearch function state: ' + state);
            if (state === "SUCCESS") {
                component.set("v.boats", response.getReturnValue());
            }
            else {
                console.log('In onSearch function Failed: ' + response);
                console.info('RESPONSE', response);
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("***Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("***Unknown error");
                }
                console.log("Failed with state: " + state); 
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
    }
})