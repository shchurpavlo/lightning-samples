({
    initialize: function(component, event, helper) {
        var keys = Object.getOwnPropertyNames(component.get("v.candidate"));
        delete keys[0];
//        helper.fetchPickListVal(component, keys, 'listSkillsOptions');
//        helper.createComponents(component, event, helper);
//        setTimeout(function(){ helper.createComponents(component, event, helper); }, 100);


        helper.fetchPickListVal(component, keys, 'listSkillsOptions').then(function() {
            helper.createComponents(component, event, helper);
        }).catch(function(error) {
             console.log('Error: ' + error)
        })

    },
    handleChange: function (component, event) {
        var selectedOptionsList = event.getParam("value");
        var targetName = event.getSource().get("v.name");
        if(targetName == 'Skills'){component.set("v.selectedSkillsItems" , selectedOptionsList);}
    },
    getSelectedItems : function(component, event, helper){
        alert(component.get("v.selectedSkillsItems"));
    },
    saveAccount: function(component,event,helper){
        var record = component.get("v.candidate");
        var action = component.get("c.createSObject");
        action.setParams({obj : record});

        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var newSObject = component.get("v.candidate");
                component.set("v.candidate", newSObject);
            } else if(state == "ERROR"){
                component.set("v.message", 'eee ' + response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    }
})