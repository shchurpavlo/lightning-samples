({
	 handleClick: function(component, event, helper) {
        var mainDiv = component.find('main-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
      },

      handleMouseLeave: function(component, event, helper) {
        component.set("v.dropdownOver",false);
        var mainDiv = component.find('main-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
      },

      handleMouseEnter: function(component, event, helper) {
        component.set("v.dropdownOver",true);
      },

      handleMouseOutButton: function(component, event, helper) {
        window.setTimeout(
          $A.getCallback(function() {
            if (component.isValid()) {
              if (component.get("v.dropdownOver")) {
                return;
              }
              var mainDiv = component.find('main-div');
              $A.util.removeClass(mainDiv, 'slds-is-open');
            }
          }), 200
        );
      }
})