({
    doInit: function(component, event, helper) {
        helper.fetchAccounts(component, event, 'Name');
    },

    sortFirstName: function(component, event, helper) {
       component.set("v.selectedTabsoft", 'firstName');
       helper.sortHelper(component, event, 'FirstName');
    },

    sortName: function(component, event, helper) {
       component.set("v.selectedTabsoft", 'Name');
       helper.sortHelper(component, event, 'Name');
    },

    sortType: function(component, event, helper) {
       component.set("v.selectedTabsoft", 'Type');
       helper.sortHelper(component, event, 'Type');
    },

    openModel: function(component, event, helper) {
        component.set("v.isOpen", true);
    },

    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },

    deleteRecord: function(component, event, helper) {
        helper.deleteRecord(component, event);
    },

    openEditComponent: function(component) {
       component.set("v.isOpen_editComponent", true);
       var cmpEvent = component.getEvent("dualListBoxEvent");
       cmpEvent.setParams({"isOpen_editComponent" : true });
       cmpEvent.fire();
    },

    closeEditComponent: function(component, event, helper) {
        helper.closeEditComponent(component, event, helper);
    }
})