({
     doInit: function(component, event, helper) {
        helper.createObjectData(component, event);
    },

     Save: function(component, event, helper) {
        if (helper.validateRequired(component, event)) {
            var action = component.get("c.saveContacts");
            action.setParams({
                "ListContact": component.get("v.contactList")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.contactList", []);
                    helper.createObjectData(component, event);
                    component.set("v.message", "Contact is created successfully!");
                    component.set("v.messageType", "success");
                    helper.showMyToast(component, event, helper);
                } else {
                    component.set("v.message", 'eee ' + response.getError()[0].message);
                    component.set("v.messageType", "error");
                    console.log(response.getError()[0].message);
                    helper.showMyToast(component, event, helper);
                }
            });
            $A.enqueueAction(action);
        }
    },

    addNewRow: function(component, event, helper) {
        helper.createObjectData(component, event);
    },

    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.contactList");
        AllRowsList.splice(index, 1);
        component.set("v.contactList", AllRowsList);
    },
})