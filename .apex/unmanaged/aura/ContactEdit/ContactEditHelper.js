({
createExpense: function(component, expense) {
    this.upsertExpense(component, expense, function(a) {
        component.set("v.view", a.getReturnValue());
    });
},

upsertExpense : function(component, expense, callback) {
  var action = component.get("c.saveExpense");
  action.setParams({"expense": expense});
  if (callback) {
      action.setCallback(this, callback);
  }
  $A.enqueueAction(action);
},

showToast : function(component, event, helper) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "type" : "success",
        "title": "Success!",
        "message": "The record has been updated successfully."
    });
    toastEvent.fire();
}

})