({
callDeleteRecord : function(component, event, helper){
    helper.deleteContact(component);
},

navigateToLightningComponent:function(component,event,helper){
   helper.openModel(component,event,helper);
},

openInfoWindow:function(component,event,helper){
   component.set("v.allowInfoWindow", true);
},

backToList:function(component,event,helper){
   var evt = $A.get("e.force:navigateToComponent");
       evt.setParams({componentDef : "c:ContactsList"});
       setTimeout(function(){
       evt.fire();
       }, 100);
}
})