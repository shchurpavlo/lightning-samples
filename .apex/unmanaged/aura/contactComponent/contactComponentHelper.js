({
deleteContact : function(component){
    var recordId =  component.get("v.view.Id");
    var action = component.get("c.deleteContactById");

    action.setParams({contactId:recordId});
    action.setCallback(this, function(response) {
        component.set("v.view", response.getReturnValue());
    });
    $A.enqueueAction(action);
},

createContact: function(component, expense) {
    this.upsertContact(component, expense, function(a) {
        component.set("v.view", a.getReturnValue());
    });
},

upsertContact : function(component, expense, callback) {
  var action = component.get("c.saveContact");
  action.setParams({"expense": expense});
  if (callback) {
    action.setCallback(this, callback);}
  $A.enqueueAction(action);
},

openModel: function(component, event, helper) {
  component.set("v.isOpen", true);
},
})