({
    initialize: function(component, event, helper) {
        component.set("v.openComponent", true);
        helper.fetchPickListVal(component, helper, 'listSkillsOptions').then(function() {
            helper.createInputComponents(component, event, helper);
        }).catch(function(error) {
             console.log('Error: ' + error)
        })
    },

    handleChange: function (component, event) {
        var selectedOptionsList = event.getParam("value");
        var targetName = event.getSource().get("v.name");
        if(targetName == 'Skills'){
            component.set("v.selectedSkillsItems" , selectedOptionsList);
        }
    },

    getSelectedItems : function(component, event, helper){
        alert(component.get("v.selectedSkillsItems"));
    },

    saveSObject: function(component, event, helper){

        helper.saveObjectToDB(component, event, helper).then(function() {
            var cmpEvent = component.getEvent("dataTableEvent");
            cmpEvent.setParams({"isOpen_editComponent" : false });
            cmpEvent.fire();
        }).catch(function(error) {
           console.log('Error: ' + error);
       });
    },

    closeComponent: function(component){
        var cmpEvent = component.getEvent("dataTableEvent");
        cmpEvent.setParams({"isOpen_editComponent" : false});
        cmpEvent.fire();
    }

})