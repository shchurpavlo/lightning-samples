({
    neverUsed : function(component, event, helper) {       
        var boat = component.get("v.boat");
        var selected = component.get("v.selected");
        
        // Fire Component event for Boat tile selection
        console.log("Select button pressed " + boat);
        var boatSelectEvent = component.getEvent("boatselect");
        boatSelectEvent.setParams({"boatId" : boat.Id});
        boatSelectEvent.fire();
        
        // Fire Additional Apllication event step #6 BoatSelected
        var appEvent = $A.get("e.c:BoatSelected");
          
        appEvent.setParams({
            "boat": boat
        });
        appEvent.fire();        
    }, 
	onBoatClick : function(component, event, helper) {
        var boat = component.get('v.boat');
        //this is how you fire an application event
        var createEvent = $A.get("e.c:PlotMapMarker");
        createEvent.setParams({'sObjectId' : boat.id});
        createEvent.fire();
	}
})