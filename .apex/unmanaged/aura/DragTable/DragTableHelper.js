({
  getAccountList: function(component) {
    var action = component.get('c.getAccounts');
    var self = this;
    action.setCallback(this, function(actionResult) {
     component.set('v.recordsToDisplay', actionResult.getReturnValue());
    });
    $A.enqueueAction(action);
  }
})