({
    markStageAsComplete : function(component, event, helper){
        var numberOfLiElements = parseInt(document.getElementById("myUl").childNodes.length);
        var idValue = parseInt(event.currentTarget.id);

        if(isNaN(idValue)){
            var result = parseInt(document.getElementsByClassName("slds-path__item slds-is-current slds-is-active")[0].getAttribute("id")) + 1;
            if(result >= numberOfLiElements){
                idValue = numberOfLiElements - 1;
            } else {
                idValue = result;
            }
        }

        helper.changeStatus(idValue, numberOfLiElements);
    },

    markStatusAsComplete : function(component, event, helper){
        var numberOfLiElements = parseInt(document.getElementById("myUl").childNodes.length);
        helper.markStatusAsComplete(numberOfLiElements);
    },

    doInit : function(component) {
        var stageNameToStageStatus = new Map();
        stageNameToStageStatus.set('Contacted', 'slds-path__item slds-is-current slds-is-active');
        stageNameToStageStatus.set('Open', 'slds-path__item slds-is-incomplete');
        stageNameToStageStatus.set('Unqualified', 'slds-path__item slds-is-incomplete');
        stageNameToStageStatus.set('Nurturing', 'slds-path__item slds-is-incomplete');
        stageNameToStageStatus.set('Closed', 'slds-path__item slds-is-incomplete');

        var newComponents = [];
        var idValue = 0;

        stageNameToStageStatus.forEach(function(value, key) {
            var tabIndex = '';
            var ariaSelected = '';
            if(value == 'slds-path__item slds-is-current slds-is-active'){
                tabIndex = "-1";
                ariaSelected = 'true'
            } else {
                tabIndex = "0";
                ariaSelected = 'false';
            }

            newComponents.push(["aura:html", {"tag":"li", "HTMLAttributes":{"id" : idValue,
                                              "class" :  value,
                                              "role" : "presentation",
                                              "onclick": component.getReference("c.markStageAsComplete")}}]);

            newComponents.push(["aura:html", {"tag":"a", "HTMLAttributes":{"aria-selected" : ariaSelected,
                                              "class" : "slds-path__link",
                                              "id" : "path-1",
                                              "role" : "option",
                                              "tabindex" : tabIndex}}]);

            newComponents.push(["aura:html", {"tag":"span", "HTMLAttributes":{"class" : "slds-path__stage"}}]);
            newComponents.push(["aura:html", {"tag":"span", "body" : "Stage Complete", "HTMLAttributes":{"class" : "slds-assistive-text"}}]);
            newComponents.push(["aura:html", {"tag":"span", "body" : key, "HTMLAttributes":{"class" : "slds-path__title"}}]);

            idValue++;
        });

        $A.createComponents(newComponents,
            function (components, status, errorMessage) {
                if (status === "SUCCESS") {
                    var pageBody = component.get("v.body");
                    for (var i = 0; i < components.length; i += 5) {
                        var li = components[i];
                        var a = components[i + 1];
                        var spanStage = components[i + 2];
                        var spanText = components[i + 3];
                        var spanTitle = components[i + 4];

                        var spanStageBody = spanStage.get("v.body");
                        spanStageBody.push(spanText);
                        spanStage.set("v.body", spanStageBody);

                        var aBody = a.get("v.body");
                        aBody.push("v.body", spanTitle);
                        a.set("v.body", aBody);

                        aBody.push("v.body", spanStage);
                        a.set("v.body", aBody);

                        var liBody = li.get("v.body");
                        liBody.push("v.body", a);
                        li.set("v.body", liBody);

                        pageBody.push(li);
                    }
                    component.set("v.body", pageBody);
                } else {
                    this.displayToast("Error", "Failed to create list components.");
                }
            }
        );
    },
})