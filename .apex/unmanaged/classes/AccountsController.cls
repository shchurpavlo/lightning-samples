public class AccountsController {
	@AuraEnabled
	public static List<Account> getAccounts() {
		return [SELECT Id, Name, Industry, Type, NumberOfEmployees, TickerSymbol, Phone FROM Account ORDER BY createdDate ASC];
	}

	@AuraEnabled
	public static List<Contact> getContacts() {
		return [SELECT Id, Name, Phone FROM Contact ORDER BY createdDate ASC];
	}
}