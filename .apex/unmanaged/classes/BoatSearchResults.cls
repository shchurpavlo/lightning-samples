public class BoatSearchResults
{
    @AuraEnabled
    public static List<Boat__c> getBoats(string boatTypeId )
    {
        list<Boat__c> boatlist=new list<Boat__c>();
        if(!String.isBlank(boatTypeId) && boatTypeId!='All') {
            boatlist=[SELECT id, BoatType__c, picture__c, name,contact__r.Name,BoatType__r.name
                      FROM Boat__c
                      WHERE BoatType__r.name =: boatTypeId ];
            return boatlist;
        }else {
            boatlist=[SELECT id, BoatType__c, picture__c, name,contact__r.Name
                      FROM Boat__c];
            return boatlist;
        }
    }
    
    @AuraEnabled
    public static string returnBoatId(Boat__c Boat)
    {
        string BoaTId=Boat.id;
        return BoaTId;
    }}