public with sharing class addDeleteController {
	@AuraEnabled
	public static void saveContacts(List<Contact> ListContact){
		try {
			insert ListContact;
		}
		catch (Exception ex){
			throw new AuraHandledException(ex.getMessage());
		}
	}
}