public class DualListboxController {

	public static final String FIELD_TYPE_PICKLIST = 'picklist';
	public static final String FIELD_TYPE_MULTIPICKLIST = 'multipicklist';

	@AuraEnabled
	public static Map<String, List<String>> getOptions(sObject obj, List<String> fieldNames) {
		String result = null;

		Map<String, Schema.SObjectField> fieldMap = obj.getSObjectType().getDescribe().fields.getMap();
		for(String key: fieldNames) {
			if(key != null) {
				Schema.SObjectField field = fieldMap.get(key);
				if(field != null) {
					if(String.valueOf(field.getDescribe().getType()) == FIELD_TYPE_MULTIPICKLIST) {
						result = String.valueOf(field);
					}
				}
			}
		}

		List<Schema.PicklistEntry> values = fieldMap.get(result).getDescribe().getPickListValues();

		return generateFieldNameToMultipicklistValuesMap(values, result);
	}

	@AuraEnabled
	public static List<FieldWrapper> getFieldsForInput(sObject obj, List<String> fieldNames) {
		String result = null;

		List<FieldWrapper> fields = new List<FieldWrapper>();
		Map<String, Schema.SObjectField> fieldMap = obj.getSObjectType().getDescribe().fields.getMap();
		for(String key: fieldNames) {
			if(key != null) {
				Schema.SObjectField field = fieldMap.get(key);
				if(field != null) {
					result = String.valueOf(field.getDescribe().getType());
					FieldWrapper fieldWrap  = new FieldWrapper();
					if(result == FIELD_TYPE_PICKLIST){
						fieldWrap.type = result;
						fieldWrap.label = key;
						List<String> options = new List<String>();
						for (Schema.PicklistEntry a: fieldMap.get(String.valueOf(field)).getDescribe().getPickListValues()) {
							options.add(a.getValue());
						}

						fieldWrap.picklistOptions = options;
						fields.add(fieldWrap);
					} else {
						fieldWrap.type = result;
						fieldWrap.label = key;
						fieldWrap.isRequired = (field.getDescribe().isNillable() == true) ? false : true;
						fields.add(fieldWrap);
					}
				}
			}
		}
		return fields;
	}

	@AuraEnabled
	public static void createSObject(sObject obj){
		try {
			insert obj;
			System.debug('createSObject');
		} catch (Exception ex) {
			System.debug('Exception createSObject');
			throw new AuraHandledException(ex.getMessage());
		}
	}

	public static Map<String, List<String>> generateFieldNameToMultipicklistValuesMap(List<Schema.PicklistEntry> values, String result) {
		Map<String, List<String>> fieldNameToMultipicklistValues = new Map<String, List<String>>();
		for (Schema.PicklistEntry a: values) {
			if(fieldNameToMultipicklistValues.get(result) == null) {
				fieldNameToMultipicklistValues.put(result, new List<String>());
			}
			fieldNameToMultipicklistValues.get(result).add(a.getValue());
		}
		if(result == null) {
			return null;
		}
		return fieldNameToMultipicklistValues;

	}

	public class FieldWrapper{
		@AuraEnabled public String type {get;set;}
		@AuraEnabled public String label {get;set;}
		@AuraEnabled public List<String> picklistOptions {get;set;}
		@AuraEnabled public Boolean isRequired {get;set;}
	}

}