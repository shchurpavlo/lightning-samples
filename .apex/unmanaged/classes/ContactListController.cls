public with sharing class ContactListController {

	@AuraEnabled
	public static Contact saveContact(Contact expense) {
		upsert expense;
		return expense;
	}

	@Auraenabled
	public static void deleteContactById(String contactId) {
		DELETE [SELECT Id FROM Contact WHERE id =:contactId];
	}

	@AuraEnabled
	public static ContactPagerWrapper fetchContact(Decimal pageNumber ,Integer recordToDisply) {
		Integer pageSize = recordToDisply;
		Integer offset = ((Integer)pageNumber - 1) * pageSize;

		ContactPagerWrapper obj =  new ContactPagerWrapper();
		obj.pageSize = pageSize;
		obj.page = (Integer) pageNumber;
		obj.total = [SELECT count() FROM Contact];
		obj.Contacts = [SELECT Id, Name, Phone FROM Contact ORDER BY Name LIMIT :recordToDisply OFFSET :offset];
		return obj;
	}

	public class ContactPagerWrapper {
		@AuraEnabled public Integer pageSize {get;set;}
		@AuraEnabled public Integer page {get;set;}
		@AuraEnabled public Integer total {get;set;}
		@AuraEnabled public List<Contact> Contacts {get;set;}
	}


}