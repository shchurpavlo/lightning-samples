public class InlineEditController {

	@AuraEnabled
	public static List <Account> fetchAccount() {

		List <Account> returnList = new List <Account> ();
		List <Account> lstOfAccount = [SELECT id, Name, Rating, Website FROM Account LIMIT 5];

		for (Account acc: lstOfAccount) {
			returnList.add(acc);
		}

		return returnList;
	}

	@AuraEnabled
	public static List < account > saveAccount(List<Account> lstAccount) {
		update lstAccount;
		return lstAccount;
	}

	@AuraEnabled
	public static List < String > getselectOptions(sObject objObject, string fld) {
		List < String > allOpts = new list < String > ();
		Schema.sObjectType objType = objObject.getSObjectType();

		Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

		map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();

		list < Schema.PicklistEntry > values =
				fieldMap.get(fld).getDescribe().getPickListValues();

		for (Schema.PicklistEntry a: values) {
			allOpts.add(a.getValue());
		}
		allOpts.sort();
		return allOpts;
	}
}
