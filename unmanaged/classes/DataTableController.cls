public class DataTableController {
	@AuraEnabled

	public static List <Account> fetchAccounts(String sortField, boolean isAsc) {
		List<Account> lstResult;
		try {
			lstResult = Database.query(prepareQueryString(sortField, isAsc));
			List<Account> returnAccountList = new List<Account>();

			for (Account c: lstResult) {
				returnAccountList.add(c);
			}
			return returnAccountList;
		}
		catch (Exception ex) {
			System.debug('ex: ' + ex.getMessage());
			return null;
		}
	}

	@Auraenabled
	public static List<Account> deleteRecordById(String recordId){
		DELETE [Select Id from Account where id=: recordId];
		return [Select ID, Name, Phone from Account ORDER BY CREATEDDATE DESC LIMIT 10];
	}

	public static String prepareQueryString(String sortField, boolean isAsc) {
		String sSoql = 'SELECT Id, Name, Type, Industry, Phone, Fax ';
		sSoql += 'FROM Account';

		if (sortField != '' && sortField != null) {
			sSoql += ' order by ' + sortField;

			if (isAsc) {
				sSoql += ' asc';
			} else {
				sSoql += ' desc';
			}
		}
		sSoql += ' LIMIT 20';

		return sSoql;
	}
}