({
	fetchPickListVal: function(component, helper, targetAttribute) {
        var opts = [];
        var picklistFields = [];
	    var fieldNames = this.getFieldNames(component);
	    return new Promise(function (resolve, reject) {
	            var action = component.get("c.getOptions");
                action.setParams({
                    "obj": component.get("v.newObject"),
                    "fieldNames": fieldNames
                });
                action.setCallback(this, function(response) {
                    if (response.getState() == "SUCCESS") {
                        resolve();
                        var allValues = response.getReturnValue();
                        if(allValues != null) {
                        for(var key in allValues) {
                            picklistFields.push(key);
                            component.set("v.multipicklistFields", picklistFields);
                            for(var index in allValues[key]) {
                                opts.push({label: allValues[key][index], value: allValues[key][index]});
                                component.set("v." + targetAttribute, opts);
                                component.set("v.hasMultipicklist", true);
                                component.set("v.showForm", true);
                            }
                        }
                        };
                    } else {
                        reject();
                        helper.showToast("error", "Error!", "There are no fields to display.");
                    }
                });
                $A.enqueueAction(action);
	    })
    },

    createInputComponents: function(component, event, helper) {
        var newComponents = [];
        var componentKey = '';

        helper.getFieldTypeToFieldName(component, helper).then(function() {
            var fieldTypeToFieldNames = component.get("v.fieldWrapper");

            return new Promise(function (resolve, reject) {

            for(var key in fieldTypeToFieldNames){
                if(fieldTypeToFieldNames[key]['type'] == 'PICKLIST'){
                    componentKey = 'ui:inputSelect';
                } else {
                    componentKey = 'lightning:input';
                }

                    if(fieldTypeToFieldNames[key]['label'] != component.get("v.multipicklistFields")[0]) {
                        newComponents.push(["aura:html", {"tag":"span", "HTMLAttributes":{}}]);
                        newComponents.push(["aura:html", {"tag":"div", "HTMLAttributes":{"class" : "container-fluid"}}]);
                        newComponents.push(["aura:html", {"tag":"div", "HTMLAttributes":{"class" : "form-group"}}]);

                        var filedLabel = helper.prepareLabel(fieldTypeToFieldNames[key]['label']);

                        if(fieldTypeToFieldNames[key]['type'] == 'PICKLIST'){
                            var opts = [];

                         for(var option in fieldTypeToFieldNames[key]['picklistOptions']){
                            opts.push({ class: "optionClass", label: fieldTypeToFieldNames[key]['picklistOptions'][option], value: fieldTypeToFieldNames[key]['picklistOptions'][option], selected: "true" })
                         };

                            newComponents.push([componentKey, {value: component.getReference("v.newObject." + fieldTypeToFieldNames[key]['label']),
                                                                    label: filedLabel,
                                                                    type: "Text",
                                                                    options : opts}]);
                        } else {
                            newComponents.push([componentKey, {value: component.getReference("v.newObject." + fieldTypeToFieldNames[key]['label']),
                                                                    label: filedLabel,
                                                                    type: "Text",
                                                                    required: fieldTypeToFieldNames[key]['isRequired']}]);
                        }

                    newComponents.push(["aura:html", {"tag":"div", "HTMLAttributes":{"class" : "col-md-4 text-center slds-m-top_x-small"}}]);
                    newComponents.push(["ui:button", {"aura:id": "findableAuraId",
                                                       "label": "Create",
                                                       "press": component.getReference("c.saveSObject")
                                                         }]);
                    newComponents.push(["ui:button", {"aura:id": "findableAuraId",
                                                       "label": "Cancel",
                                                       "press": component.getReference("c.closeComponent")
                                                         }]);
                }

            }
            $A.createComponents(newComponents, function (components, status, errorMessage) {

                var numberOfElements = 7
                if (status === "SUCCESS") {
                    for (var i = 0; i < components.length; i += numberOfElements) {

                        var span_1 = components[i];
                        var div_fluid = components[i + 1];
                        var div_form = components[i + 2];
                        var ui = components[i + 3];
                        var div_button = components[i + 4];
                        var buttonCreate = components[i + 5];
                        var buttonCancel = components[i + 6];

                        var div_formBody = div_form.get("v.body");
                        div_formBody.push(ui);
                        div_form.set("v.body", div_formBody);

                        var div_fluidBody = div_fluid.get("v.body");
                        div_fluidBody.push(div_form);
                        div_fluid.set("v.body", div_fluidBody);

                        var span_1Body = span_1.get("v.body");
                        span_1Body.push(div_fluid);
                        span_1.set("v.body", span_1Body);

                        var div_buttonBody = div_button.get("v.body");
                        div_buttonBody.push(buttonCreate);
                        div_button.set("v.body", div_buttonBody);

                        var div_buttonBody = div_button.get("v.body");
                        div_buttonBody.push(buttonCancel);
                        div_button.set("v.body", div_buttonBody);

                        if(i == components.length - numberOfElements) {
                            var span_1Body = span_1.get("v.body");
                            span_1Body.push(div_button);
                            span_1.set("v.body", span_1Body);
                        }

                        var pageBody = component.get("v.body");
                        pageBody.push(span_1);
                        component.set("v.body", pageBody);
                    }
                    resolve();
                } else {
                    reject();
                    helper.showToast("error", "Error!", "There are no fields to display.");
                }
                }
            );
        });
        }).catch(function(error) {
             console.log('Error: ' + error)
        })
    },

    getFieldNames : function(component) {
        var keys = Object.getOwnPropertyNames(component.get("v.newObject"));
        keys.splice(0, 1);
        return keys;
    },

    getFieldTypeToFieldName : function(component, helper) {
        var keys = Object.getOwnPropertyNames(component.get("v.newObject"));
        keys.splice(0, 1);

        var fieldNames = [];

        for(var index in keys) {fieldNames.push(keys[index]);}

        return new Promise(function (resolve, reject) {
            var action = component.get("c.getFieldsForInput");

            action.setParams({
                "obj": component.get("v.newObject"),
                "fieldNames": fieldNames
            });

            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    resolve();
                    var allValues = response.getReturnValue();
                    if(allValues != null) {
                        component.set("v.fieldWrapper", allValues);
                    };
                } else {
                    reject();
                    helper.showToast("error", "Error!", "ERROR.");
                }
            });
            $A.enqueueAction(action);
        });
    },


    saveObjectToDB: function(component, event, helper){
        return new Promise(function (resolve, reject) {

            var record = component.get("v.newObject");
            var action = component.get("c.createSObject");

            if(helper.checkInputCorrectness(component, record) == true) {

                action.setParams({
                    obj : record
                });

                action.setCallback(this, function(response){
                    console.log('setCallback');
                    var state = response.getState();
                    if(state == "SUCCESS"){
                        resolve(component);
                        helper.showToast("success", null, `Account ${record['Name']} was created`);
                        component.set("v.newObject", helper.prepareNewSObject(record));
                    } else if(state == "ERROR"){
                        reject();
                        console.log('error');
                        component.set("v.message", response.getError()[0].message);
                    }
                });
                $A.enqueueAction(action);
            } else {
                helper.showToast("error", "Error!", `Fields with * should be filled`);
            }
        })
    },

    checkInputCorrectness: function(component, record) {
        var result = null;
        var fieldTypeToFieldNames = component.get("v.fieldWrapper");
        for(var key in fieldTypeToFieldNames) {
            if(fieldTypeToFieldNames[key]['isRequired'] == true) {
               if(record[fieldTypeToFieldNames[key]['label']] == '') {
                    result = false;
               } else {
                    result = true;
               }
            }
        }
        return result;
    },

    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message,
        });
        toastEvent.fire();
    },

    prepareLabel: function(labelName) {
        var result = String(labelName);
        if(result.endsWith('__c')) {result = result.replace('__c', '');}
        return result;
    },

    prepareNewSObject : function(record) {
        for(var key in record) {
            if(key != 'sobjectType') {
                delete record[key];
            }
        }
        return record;
    }
})