({
    changeStatus : function(idValue, numberOfLiElements) {
        for(var i = 0; i < numberOfLiElements; i++){
            if(parseInt(i) == idValue){
                document.getElementById(idValue).removeAttribute("class");
                document.getElementById(idValue).setAttribute("class", "slds-path__item slds-is-current slds-is-active");
                document.querySelector('.slds-path__link').setAttribute("aria-selected", "true");
                document.querySelector('.slds-path__link').setAttribute("tabindex", "0");
            }
            if(parseInt(i) < idValue){
                document.getElementById(i).removeAttribute("class");
                document.getElementById(i).setAttribute("class", "slds-path__item slds-is-complete");
                document.querySelector('.slds-path__link').setAttribute("aria-selected", "false");
                document.querySelector('.slds-path__link').setAttribute("tabindex", "-1");
            }
            if(parseInt(i) > idValue){
                document.getElementById(i).removeAttribute("class");
                document.getElementById(i).setAttribute("class", "slds-path__item slds-is-incomplete");
                document.querySelector('.slds-path__link').setAttribute("aria-selected", "false");
                document.querySelector('.slds-path__link').setAttribute("tabindex", "-1");
            }
        }
    },

    isActive : function(idValue, numberOfLiElements){
        var result = false;
        if(idValue >= 0 && idValue < numberOfLiElements && document.getElementById(idValue).className == 'slds-path__item slds-is-current slds-is-active') {
            result = true;
        }
        return result;
    },
    isComplete : function(idValue, numberOfLiElements){
        var result = false;
        if(document.getElementById(idValue).className == 'slds-path__item slds-is-complete') {
            result = true;
        }
        return result;
    },
    isIncomplete : function(idValue, numberOfLiElements){
        var result = false;
        if(idValue < numberOfLiElements && document.getElementById(idValue).className == 'slds-path__item slds-is-incomplete' || idValue == numberOfLiElements - 1) {
            result = true;
        }
        return result;
    },
    markStatusAsComplete : function(numberOfLiElements) {
        for(var i = 0; i < numberOfLiElements; i++) {
            document.getElementById(i).removeAttribute("class");
            document.getElementById(i).setAttribute("class", "slds-path__item slds-is-complete");
            document.querySelector('.slds-path__link').setAttribute("aria-selected", "false");
            document.querySelector('.slds-path__link').setAttribute("tabindex", "-1");
        }
    }
})