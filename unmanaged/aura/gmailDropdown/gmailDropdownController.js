({
	init: function(component, event, helper) {
       var options = component.get("v.options");
       options.sort(function compare(a,b) {
      if (a.value == 'All'){
        return -1;
      }
      else if (a.value < b.value){
        return -1;
      }
      if (a.value > b.value){
        return 1;
      }
      return 0;
    });

       component.set("v.options_",options);
       var values = helper.getSelectedValues(component);
       helper.setInfoText(component,values);
     },

     handleSelection: function(component, event, helper) {
       var item = event.currentTarget;
       if (item && item.dataset) {
         var value = item.dataset.value;
         var selected = item.dataset.selected;

         var options = component.get("v.options_");

         if (event.shiftKey) {
           options.forEach(function(element) {
             if (element.value == value) {
               element.selected = selected == "true" ? false : true;
             }
           });
         } else {
           options.forEach(function(element) {
             if (element.value == value) {
               element.selected = selected == "true" ? false : true;
             } else {
               element.selected = false;
             }
           });
           var mainDiv = component.find('main-div');
           $A.util.removeClass(mainDiv, 'slds-is-open');
         }
         component.set("v.options_", options);
         var values = helper.getSelectedValues(component);
         var labels = helper.getSelectedLabels(component);

         helper.setInfoText(component,values);
         helper.despatchSelectChangeEvent(component,labels);

       }
     },
})