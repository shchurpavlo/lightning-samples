({
    createObjectData: function(component, event) {
        var RowItemList = component.get("v.contactList");
        RowItemList.push({
            'sobjectType': 'Contact',
            'FirstName': '',
            'LastName': '',
            'Phone': ''
        });
        component.set("v.contactList", RowItemList);
    },

    validateRequired: function(component, event) {
        var isValid = true;
        var allContactRows = component.get("v.contactList");
        for (var indexVar = 0; indexVar < allContactRows.length; indexVar++) {
            console.log('allContactRows : ' + allContactRows[indexVar].FirstName);
            if (allContactRows[indexVar].LastName != '') {
                isValid = false;
                component.set("v.message", 'Last Name Can\'t be Blank on Row Number ' + (indexVar + 1));
                component.set("v.messageType", "error");
                this.showMyToast(component, event);
            }
        }
        return isValid;
    },

    showMyToast : function(component, event) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                type: component.get("v.messageType"),
                mode: 'sticky',
                message: component.get("v.message"),
            });
            toastEvent.fire();
        }
})