({
  setInfoText: function(component, labels) {
    if (labels.length == 0) { component.set("v.infoText", "Select an option..."); }
    if (labels.length == 1) { component.set("v.infoText", labels[0]); }
    else if (labels.length > 1) { component.set("v.infoText", labels.length + " options selected"); }
  },

  getSelectedValues: function(component){
    var values = [];

    var options = component.get("v.options_");

    options.forEach(function(element) {
      if (element.selected) { values.push(element.value); }
    });
    return values;
  },

  getSelectedLabels: function(component){
    var labels = [];
    var options = component.get("v.options_");
    options.forEach(function(element) {
      if (element.selected) {
        labels.push(element.label);
      }
    });
    return labels;
  },

  despatchSelectChangeEvent: function(component, values){
    var compEvent = component.getEvent("selectChange");
    compEvent.setParams({ "values": values });
    compEvent.fire();
  },

  handleClick: function(component, event, helper) {
    var mainDiv = component.find('main-div');
    $A.util.addClass(mainDiv, 'slds-is-open');
  },

  sort: function(component, event, helper) {
    var result = null;
    if(document.getElementById('combobox-unique-id-2') != null) {
       result = document.getElementById('combobox-unique-id-2').value;
    }
    return result;
  },

  toggleAccountTable : function(component, options) {
    var objectTypes = [];
    options.forEach(function(option){

        if(String(option.value) == 'Initial Contact' && option.selected == true) {
            objectTypes.push("Contact");
            component.set('v.allowRecordsDisplay', true);
        }

        if(String(option.value) == 'Initial Account' && option.selected == true) {
            objectTypes.push("Account");
            component.set('v.allowRecordsDisplay', true);
        }
    });
    return objectTypes;
  },

  toastNoRecords : function(component, event, helper) {
       var toastEvent = $A.get("e.force:showToast");
       toastEvent.setParams({
           type: 'error',
           title: 'Error!',
           message: 'There are no records to display.'
       });
       toastEvent.fire();
    }
})