({
    fetchAccounts: function(component, event, sortFieldName) {
       var action = component.get("c.fetchAccounts");

       action.setParams({
                'sortField': sortFieldName,
                'isAsc': component.get("v.isAsc")
       });

       action.setCallback(this, function(response) {
           var state = response.getState();
           if (state === "SUCCESS") {
               var storeResponse = response.getReturnValue();
               component.set("v.searchResult", storeResponse);
           }

       });
       $A.enqueueAction(action);
    },

	deleteRecord : function(component, event) {
        var action = component.get("c.deleteRecordById");
        action.setParams({recordId:event.target.id});
        action.setCallback(this, function(response) {
            component.set("v.searchResult", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    sortHelper: function(component, event, sortFieldName) {
        var currentDir = component.get("v.arrowDirection");

        if (currentDir == 'arrowdown') {
         component.set("v.arrowDirection", 'arrowup');
         component.set("v.isAsc", true);
        } else {
         component.set("v.arrowDirection", 'arrowdown');
         component.set("v.isAsc", false);
        }
        this.fetchAccounts(component, event, sortFieldName);
    },

    closeEditComponent: function(component, event, helper) {
       var isOpen_editComponent = event.getParam("isOpen_editComponent");
       component.set("v.isOpen_editComponent", isOpen_editComponent);
       helper.fetchAccounts(component, event);
    }
})