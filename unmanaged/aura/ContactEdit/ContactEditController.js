({
navigateToLightningComponent:function(component, event, helper){
console.log('navigateToLightningComponent');
    var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({componentDef : "c:ContactsList"});
        setTimeout(function(){evt.fire();}, 100);

    helper.showToast();
},

closeEditWindow : function(component, event, helper){
     var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({componentDef : "c:ContactsList",
        componentAttributes: {isOpen : "false"}});
        setTimeout(function(){
        evt.fire();
        }, 100);
}
})