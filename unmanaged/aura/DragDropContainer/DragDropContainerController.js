({
    doInit: function(component, event, helper) {        
        var newItems = [];

        var action = component.get("c.fetchAccount");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                for(var key in storeResponse) {
                    console.log('DragDropContainerController doInit: ' + storeResponse[key]['Name']);
                    newItems.push({title: storeResponse[key]['Name'],
                                    id: "23243342",
                                    status: "Name"
                                    });
            	}
            component.set("v.allItems", newItems);
            }
        });
        $A.enqueueAction(action);

    },

   onPipelineChanged: function(component, event, helper) {
    var title = event.getParam("title");
    var items = event.getParam("items");
    var allLists = component.get("v.allItems");
       for(var key in items) {
   			allLists.find(function(el) {
                if(el.id == items[key].id) {
                    el.status = title;
                    component.set("v.allItems", allLists);
                } else {
                    console.log("could not find item ", items[key], " in list ", allLists);
                }   
   			 });    
       }   
  }
   
});