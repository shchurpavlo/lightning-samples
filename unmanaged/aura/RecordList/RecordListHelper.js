({
  getAccountList: function(component, event) {
     var param = [];
     var action;
     var params = event.getParam('arguments');
     if (params) { param = params.param; }

    param.forEach(function(paramValue){
        if(paramValue == 'Contact'){action = component.get('c.getContacts');}
        if(paramValue == 'Account'){action = component.get('c.getAccounts');}
    });

    if (param !== null) {
        var self = this;
        action.setCallback(this, function(actionResult) {
         if(actionResult.getReturnValue() == null) {
            this.toastNoRecords();
         } else {
            component.set('v.records', actionResult.getReturnValue());
         }
        });
        $A.enqueueAction(action);
    }
  },

  toastNoRecords : function(component, event, helper) {
       var toastEvent = $A.get("e.force:showToast");
       toastEvent.setParams({
           type: 'error',
           title: 'Error!',
           message: 'There are no records to display.'
       });
       toastEvent.fire();
  }
})