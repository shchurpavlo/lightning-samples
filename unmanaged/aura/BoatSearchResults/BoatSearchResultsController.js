({
     doSearch: function(component, event, helper) 
    {
        helper.onSearch(component, event, helper);
    },
    search: function(component, event, helper)
    {
        console.log("BSRController: search called");
        console.log('selectboatdid '+component.get("v.selectedBoatId"));
        console.log('boats object attribute '+JSON.stringify(component.get("v.boats")));
        var params = event.getParam('arguments');
        console.log("boatTypeId extracted: " + params.boatTypeId);
        component.set("v.boatTypeId", params.boatTypeId);
        helper.onSearch(component, event, helper);
        return "search complete.";
    },
    onBoatSelect : function(component, event, helper) 
    {
        var boatId = event.getParam("boatId");
        debugger
        console.log('Boat id got from event'+boatId);
        component.set("v.selectedBoatId",boatId);
        console.log('boats object attribute '+JSON.stringify(component.get("v.boats")));
        console.log('atrribute value set '+component.get("v.selectedBoatId"));
    }
})