({
sortBy: function(component, field) {
    var sortAsc = component.get("v.sortAsc"),
        sortField = component.get("v.sortField"),
        records = component.get("v.manageView");
    sortAsc = field == sortField ? !sortAsc: true;
    records.sort(function(a,b){
        var t1 = a[field] == b[field],
            t2 = a[field] > b[field];
        return t1? 0: (sortAsc ? - 1 : 1) * (t2 ? - 1 : 1);
    });
    component.set("v.sortAsc", sortAsc);
    component.set("v.sortField", field);
    component.set("v.manageView", records);
},

getContacts: function(component, page, recordToDisply) {

      var action = component.get("c.fetchContact");
      action.setParams({"pageNumber": page, "recordToDisply": recordToDisply});
      action.setCallback(this, function(a) {
         var result = a.getReturnValue();
         console.log('result.Contacts : ' + result.Contacts);
         component.set("v.manageView", result.Contacts);
         component.set("v.page", result.page);
         component.set("v.total", result.total);
         component.set("v.pages", Math.ceil(result.total / recordToDisply));
      });
      $A.enqueueAction(action);
   }
})