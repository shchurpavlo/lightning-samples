({
    doInit: function(component, event, helper) {
          var page = component.get("v.page") || 1;
          var recordToDisply = component.find("recordSize").get("v.value");
          helper.getContacts(component, page, recordToDisply);

   },

    sortByName: function(component, event, helper) {
        helper.sortBy(component, "Name");
},

    sortByEmail: function(component, event, helper) {
        helper.sortBy(component, "Email");
},

    navigate: function(component, event, helper) {
          var page = component.get("v.page") || 1;
          var direction = event.getSource().get("v.label");
          var recordToDisply = component.find("recordSize").get("v.value");
          page = direction === "Previous Page" ? (page - 1) : (page + 1);
          helper.getContacts(component, page, recordToDisply);
},

    onSelectChange: function(component, event, helper) {
      var page = 1
      var recordToDisply = component.find("recordSize").get("v.value");
      helper.getContacts(component, page, recordToDisply);
},

    fireApplicationEvent : function(cmp, event) {
        var appEvent = $A.get("e.c:aeEvent");
        appEvent.setParams({
            "message" : "An application event fired me. " +
            "It all happened so fast. Now, I'm everywhere!" });
        appEvent.fire();
        console.log('fireApplicationEvent');
    }
})