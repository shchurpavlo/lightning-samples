({
    allowDrop: function(component, event, helper) {
        console.log('pipelineController allowDrop');
        event.preventDefault();
    },

    onDragStart : function(component, event, helper) {
         event.dataTransfer.dropEffect = "move";
         var item = component.get('v.items');
         event.dataTransfer.setData('text', JSON.stringify(item));
    },
    
    onDrop: function(component, event, helper) {
        event.preventDefault();
        var pipelineChangeEvent = component.getEvent('pipelineChange');
        pipelineChangeEvent.setParams({
            'title': component.get('v.title'),
            'items': JSON.parse(event.dataTransfer.getData('text'))
        });
        pipelineChangeEvent.fire();
    },


})