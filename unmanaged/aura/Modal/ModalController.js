({
    doInit : function(component, event) {

        var textVariable = 'Sit nulla est ex deserunt exercitation anim occaecat. Nostrud ullamco deserunt aute id ' +
                            'consequat veniam incididunt duis in sint irure nisi. Mollit officia cillum Lorem ullamco minim nostrud elit officia tempor esse quis. Cillum sunt ad dolore ' +
                                      'quis aute consequat ipsum magna exercitation reprehenderit magna. Tempor cupidatat consequat elit dolor adipisicing.';
        var allowHeader = true;
        var allowFooter = true;

        var newComponents = [];

        newComponents.push(["aura:html", {"tag":"div", "HTMLAttributes":{"aura:id" : "modal", "class" :  'demo-only',
                                          "style" : "height: 640px;"}}]);

        newComponents.push(["aura:html", {"tag":"section", "HTMLAttributes":{"role" : "dialog",
                                          "tabindex" : "-1",
                                          "aria-labelledby" : "modal-heading-01",
                                          "aria-modal" : "true",
                                          "aria-describedby" : "modal-content-id-1",
                                          "class" : "slds-modal slds-fade-in-open"}}]);

        newComponents.push(["aura:html", {"tag":"div", "HTMLAttributes":{"class" : "slds-modal__container"}}]);
        newComponents.push(["aura:html", {"tag":"header", "HTMLAttributes":{"class" : "slds-modal__header"}}]);
        newComponents.push(["aura:html", {"tag":"div", "HTMLAttributes":{"class" : "slds-button slds-button_icon slds-modal__close slds-button_icon-inverse",
                                           "title" : "Close"}}]);
        newComponents.push(["aura:html", {"tag":"span", "body" : "Close", "HTMLAttributes":{"class" : "slds-assistive-text"}}]);
        newComponents.push(["aura:html", {"tag":"h2", "body" : "Modal Header", "HTMLAttributes":{"id" : "modal-heading-01",
                                          "class" : "slds-text-heading_medium slds-hyphenate"}}]);
        newComponents.push(["aura:html", {"tag" : "div", "HTMLAttributes":{"class" : "slds-modal__content slds-p-around_medium",
                                          "id" : "modal-content-id-1"}}]);
        newComponents.push(["aura:html", {"tag" : "p", "body" : component.get('v.valueFromParent'), "HTMLAttributes":{}}]);
        newComponents.push(["aura:html", {"tag" : "footer", "HTMLAttributes" : {"class" : "slds-modal__footer"}}]);

        newComponents.push(["aura:html", {"tag":"button", "body" : "Cancel", "HTMLAttributes":{"class" : "slds-button slds-button_neutral", "onclick": component.getReference("c.cancel")}}]);
        newComponents.push(["aura:html", {"tag":"button", "body" : "Save", "HTMLAttributes":{"class" : "slds-button slds-button_brand"}}]);
        newComponents.push(["aura:html", {"tag":"div", "HTMLAttributes":{"class" : "slds-backdrop slds-backdrop_open"}}]);

        $A.createComponents(newComponents,
            function (components, status, errorMessage) {
                if (status === "SUCCESS") {
                    var pageBody = component.get("v.body");
                    for (var i = 0; i < components.length; i += 13) {
                        var outerDiv = components[i];
                        var section = components[i + 1];
                        var div_1 = components[i + 2];
                        if(allowHeader){
                            var header = components[i + 3];
                        }
                        var button = components[i + 4];
                        var span_1 = components[i + 5];
                        var h2 = components[i + 6];
                        var div_2 = components[i + 7];
                        var p = components[i + 8];
                        if(allowFooter){
                            var footer = components[i + 9];
                        }
                        var button_Cancel = components[i + 10];
                        var button_Save = components[i + 11];
                        var div_3 = components[i + 12];

                        var buttonBody = button.get("v.body");
                        buttonBody.push(span_1);
                        button.set("v.body", buttonBody);

                        if(allowHeader){
                            var headerBody = header.get("v.body");
                            headerBody.push(button);
                            header.set("v.body", headerBody);

                            var headerBody = header.get("v.body");
                            headerBody.push(h2);
                            header.set("v.body", headerBody);
                        }

                        var div_2Body = div_2.get("v.body");
                        div_2Body.push("v.body", p);
                        div_2.set("v.body", div_2Body);

                        if(allowFooter){
                            var footerBody = footer.get("v.body");
                            footerBody.push("v.body", button_Cancel);
                            footer.set("v.body", footerBody);

                            var footerBody = footer.get("v.body");
                            footerBody.push("v.body", button_Save);
                            footer.set("v.body", footerBody);
                        }

                        if(allowHeader){
                            var div_1Body = div_1.get("v.body");
                            div_1Body.push("v.body", header);
                            div_1.set("v.body", div_1Body);
                        }

                        var div_1Body = div_1.get("v.body");
                        div_1Body.push("v.body", div_2);
                        div_1.set("v.body", div_1Body);

                        if(allowFooter){
                            var div_1Body = div_1.get("v.body");
                            div_1Body.push("v.body", footer);
                            div_1.set("v.body", div_1Body);
                        }

                        var sectionBody = section.get("v.body");
                        sectionBody.push("v.body", div_1);
                        section.set("v.body", sectionBody);

                        var outerDivBody = outerDiv.get("v.body");
                        outerDivBody.push("v.body", section);
                        outerDiv.set("v.body", outerDivBody);

                        var outerDivBody = outerDiv.get("v.body");
                        outerDivBody.push("v.body", div_3);
                        outerDiv.set("v.body", outerDivBody);

                        pageBody.push(outerDiv);
                    }
                    component.set("v.body", pageBody);
                } else {
                    this.displayToast("Error", "Failed to create list components.");
                }
            }
        );
    },

    cancel : function(component, event, helper) {
        component.set("v.allowModal", false);
    },
})